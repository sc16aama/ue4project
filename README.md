# UE4Project

## Setup

Initally, Unreal Engine 4 must be installed in-order to run this project. The version used in the development
of this project was 4.21.2. Earlier version of this software are not reccomended as it has not been tested on those builds.

In-order to retrieve the files, you must clone the reposirty ('https://gitlab.com/sc16aama/ue4project') which will download the project.

## Execution

Once cloned, navigate to the directory and double click on the file named WalkingAmongZombies.uproject. This will attempt
to build the project and launch unreal engine. 
Once The engine launches, you can begin the program using the play button on the top pannel. This will begin our program
but it initally begins in a paused state, to start the simulation you must press "P". the "P" key can also be pressed once again
to pause the simulation. To end the simulation simply press "ESC". 

## Controls

The camera can be moved in the X and Y directions using the "W", "A", "S" and "D" keys. The movement speed of the camera can
be increased by holding down "SHIFT" and using the "W", "A", "S" and "D" keys. 
You can also Pan the camera by holding the "middle mouse button" and moving the mouse. The "scroll wheel" can be used to zoom in and out.

The UI allows the user to simply click and place obstacles down on to the map using the "Left Mouse Button". We can deselect the obstacle
by clicking on the "Right Mouse Button".